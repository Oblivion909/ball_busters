﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Player : MonoBehaviour
{
    private Vector3 m_AccelerationDir;

    private List<Enemy_Movement> m_Enemys = new List<Enemy_Movement>();


    //Clock wise begins at top left
    [SerializeField]
    private Transform[] m_LookAtPos;

    private RaycastHit m_Hit;
    private Ray m_Ray;

    private ScalingToScreen m_Scaling;
    private Cooldown m_Cooldown;

    private void Start()
    {
        m_Scaling = FindObjectOfType<ScalingToScreen>();
    }

    void Update()
    {
        m_LookAtPos[0].position = m_Scaling.m_ObjectToLocateTopLeft.transform.position;
        m_LookAtPos[1].position = m_Scaling.m_ObjectToLocateTopRight.transform.position;
        m_LookAtPos[2].position = m_Scaling.m_ObjectToLocateBottomRight.transform.position;
        m_LookAtPos[3].position = m_Scaling.m_ObjectToLocateBottomLeft.transform.position;

        m_AccelerationDir = Input.acceleration;

        if (m_AccelerationDir.sqrMagnitude >= 5f)
        {
            if(m_Enemys != null)
            {
                for (int i = 0; i < m_Enemys.Count; i++)
                {
                    m_Enemys[i].m_IsKnockbacked = true;
                }
            }            
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////// Tap Buttons
    public void UpLeftTap()
    {
        transform.LookAt(m_LookAtPos[0]);
        Debug.DrawRay(transform.position, transform.forward * 5, Color.green);

        if (Physics.Raycast(transform.position, transform.forward, out m_Hit, 5f))
        {
            if (m_Hit.collider != null)
            {
                Enemy_Health enemy = m_Hit.collider.GetComponent<Enemy_Health>();
                enemy.TakeDamage(1);
            }
            else if (m_Hit.collider == null)
            {
                m_Cooldown.m_Missed = true;
                Debug.Log("Missed");
            }
        }
    }
    public void UpRightTap()
    {
        transform.LookAt(m_LookAtPos[1]);
        Debug.DrawRay(transform.position, transform.forward * 5, Color.green);

        if (Physics.Raycast(transform.position, transform.forward, out m_Hit, 5f))
        {
            if (m_Hit.collider != null)
            {
                Enemy_Health enemy = m_Hit.collider.GetComponent<Enemy_Health>();
                enemy.TakeDamage(1);
            }
            else if (m_Hit.collider == null)
            {
                m_Cooldown.m_Missed = true;
                Debug.Log("Missed");
            }
        }
    }
    public void BottomRightTap()
    {
        transform.LookAt(m_LookAtPos[2]);
        Debug.DrawRay(transform.position, transform.forward * 5, Color.green);

        if (Physics.Raycast(transform.position, transform.forward, out m_Hit, 5f))
        {
            if (m_Hit.collider != null)
            {
                Enemy_Health enemy = m_Hit.collider.GetComponent<Enemy_Health>();
                enemy.TakeDamage(1);
            }
            else if (m_Hit.collider == null)
            {
                m_Cooldown.m_Missed = true;
                Debug.Log("Missed");
            }
        }
    }
    public void BottomLeftTap()
    {
        transform.LookAt(m_LookAtPos[3]);
        Debug.DrawRay(transform.position, transform.forward * 5, Color.green);

        if (Physics.Raycast(transform.position, transform.forward, out m_Hit, 5f))
        {
            if (m_Hit.collider != null)
            {
                Enemy_Health enemy = m_Hit.collider.GetComponent<Enemy_Health>();
                enemy.TakeDamage(1);
            }
            else if (m_Hit.collider == null)
            {
                m_Cooldown.m_Missed = true;
                Debug.Log("Missed");
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////// Swipe Buttons
    public void UpLeftSwipe()
    {
        transform.LookAt(m_LookAtPos[0]);
        Debug.DrawRay(transform.position, transform.forward * 10, Color.blue);
    }
    public void UpRightSwipe()
    {
        transform.LookAt(m_LookAtPos[1]);
        Debug.DrawRay(transform.position, transform.forward * 10, Color.blue);
    }
    public void BottomRightSwipe()
    {
        transform.LookAt(m_LookAtPos[2]);
        Debug.DrawRay(transform.position, transform.forward * 10, Color.blue);
    }
    public void BottomLeftSwipe()
    {
        transform.LookAt(m_LookAtPos[3]);
        Debug.DrawRay(transform.position, transform.forward * 10, Color.blue);
    }

    public void AddEnemy(Enemy_Movement enemys)
    {
        m_Enemys.Add(enemys);
    }
}


