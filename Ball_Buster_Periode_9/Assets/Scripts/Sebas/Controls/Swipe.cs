﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Movement
{
    public struct SwipeData
    {
        public Vector2 StartPos;
        public Vector2 EndPos;
        public SwipeDirection Direction;
    }

    public enum SwipeDirection
    {
        LeftUp,
        LeftDown,
        RightUp,
        RightDown
    }

    public class Swipe : MonoBehaviour
    {
        private Vector2 m_MousPos;

        #region Swipe Elements
        private Vector2 m_StartTouchPos;
        private Vector2 m_ReleaseTouchPos;

        [SerializeField]
        private bool m_DetectAfterRelease = false;

        [SerializeField]
        private float m_MinDistSwipe = 20;

        public static event System.Action<SwipeData> OnSwipe = delegate { };
        #endregion

        #region Tap Elements
        private Rect topLeft = new Rect(0, 0, Screen.width / 2, Screen.height / 2);
        private Rect bottomLeft = new Rect(0, Screen.height / 2, Screen.width / 2, Screen.height / 2);
        private Rect topRight = new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height / 2);
        private Rect bottomRight = new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2);
        #endregion

        private Movement_Player m_Controlls;
        private float m_Width;
        private float m_Height;

        private void Awake()
        {
            m_Width = (float)Screen.width / 2.0f;
            m_Height = (float)Screen.height / 2.0f;
        }

        private void Start()
        {
            m_Controlls = GameObject.FindObjectOfType<Movement_Player>();
        }

        void Update()
        {
            TapDetection();
            DetectSwipe();
        }


        private void TapDetection()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    //Bottom Left
                    if (touch.position.x > topLeft.x && touch.position.x < Screen.width / 2 && touch.position.y > topLeft.y && touch.position.y < Screen.height / 2)
                    {
                        m_Controlls.BottomLeftTap();
                    }

                    //Top Left
                    if (touch.position.x > topLeft.x && touch.position.x < Screen.width / 2 && touch.position.y > topLeft.y && touch.position.y > Screen.height / 2)
                    {
                        m_Controlls.UpLeftTap();
                    }

                    //Top Right
                    if (touch.position.x > Screen.width / 2 && touch.position.y > topLeft.y && touch.position.y > Screen.height / 2)
                    {
                        m_Controlls.UpRightTap();
                    }

                    if (touch.position.x > Screen.width / 2 && touch.position.y > topLeft.y && touch.position.y < Screen.height / 2)
                    {
                        m_Controlls.BottomRightTap();
                    }
                }
            }
        }

        private void DetectSwipe()
        {
            if (Input.touchCount == 1) // user is touching the screen with a single touch
            {
                Touch touch = Input.GetTouch(0); // get the touch
                if (touch.phase == TouchPhase.Began) //check for the first touch
                {
                    m_StartTouchPos = touch.position;
                    m_ReleaseTouchPos = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
                {
                    m_ReleaseTouchPos = touch.position;  //last touch position. Ommitted if you use list

                    //Check if drag distance is greater than 20% of the screen height
                    if (Mathf.Abs(m_ReleaseTouchPos.x - m_StartTouchPos.x) > m_MinDistSwipe || Mathf.Abs(m_ReleaseTouchPos.y - m_StartTouchPos.y) > m_MinDistSwipe)
                    {//It's a drag
                     //check if the drag is vertical or horizontal
                        if (Mathf.Abs(m_ReleaseTouchPos.x - m_StartTouchPos.x) > Mathf.Abs(m_ReleaseTouchPos.y - m_StartTouchPos.y))
                        {   //If the horizontal movement is greater than the vertical movement...
                            if ((m_ReleaseTouchPos.x > m_StartTouchPos.x))  //If the movement was to the right)
                            {   //Right swipe
                                Debug.Log("Right Swipe");
                            }
                            else
                            {   //Left swipe
                                Debug.Log("Left Swipe");
                            }
                        }
                        else
                        {   //the vertical movement is greater than the horizontal movement
                            if (m_ReleaseTouchPos.y > m_StartTouchPos.y)  //If the movement was up
                            {   //Up swipe
                                Debug.Log("Up Swipe");
                            }
                            else
                            {   //Down swipe
                                Debug.Log("Down Swipe");
                            }
                        }
                    }
                    else
                    {   //It's a tap as the drag distance is less than 20% of the screen height
                        Debug.Log("Tap");
                    }
                }
            }
        }
        private bool IsVerticalSwipe()
        {
            return true;
        }
    }
}

