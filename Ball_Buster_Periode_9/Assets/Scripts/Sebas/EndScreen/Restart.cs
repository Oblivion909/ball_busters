﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public string m_LevelName;

    public void RestartScene()
    {
        SceneManager.LoadScene(m_LevelName);
    }
}
