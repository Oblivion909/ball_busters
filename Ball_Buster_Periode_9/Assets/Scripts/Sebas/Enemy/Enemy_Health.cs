﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Health : MonoBehaviour
{
    private Score m_Scores;

    private SpawnSystem_Enemy m_SpawnSystem;
    private Enemy_Movement m_Enemy;

    [SerializeField]
    private int m_MaxHP;
    private int m_CurrentHP;

    [HideInInspector] public bool m_Died;

    void Start()
    {
        m_SpawnSystem = FindObjectOfType<SpawnSystem_Enemy>();
        m_Enemy = GetComponent<Enemy_Movement>();
        m_Scores = FindObjectOfType<Score>();
        m_CurrentHP = m_MaxHP;
    }

    public void TakeDamage(int damageamount)
    {
        m_CurrentHP -= damageamount;
        if (m_CurrentHP <= 0)
        {
            Die();
            m_Died = true;
            m_Scores.m_Score += m_Scores.m_ScoreAdded;
        }
        //else
        //m_Enemy.m_IsKnockbacked = true;
    }

    public void Die()
    {
        m_SpawnSystem.Respawn(transform);
        m_CurrentHP = m_MaxHP;
        m_Died = false;
    }
}


