﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Enemy_Movement : MonoBehaviour
{
    private Score m_Scores;

    public bool m_IsKnockbacked = false;

    [SerializeField]
    private float m_MultiplierKnockback = 70f;
    [SerializeField]
    private float m_Speed = 0.05f;

    [Header("Target")]
    [SerializeField]
    private GameObject m_SeekTarget;

    private Movement_Player m_Player;

    private Rigidbody m_RB;
    private Vector3 m_KnockbackDir;

    private Vector3 m_TargetPos;

    private Ray m_Ray;
    private RaycastHit m_Hit;

    private void Start()
    {
        m_SeekTarget = FindObjectOfType<Movement_Player>().gameObject;
        m_Scores = FindObjectOfType<Score>();
        m_RB = GetComponent<Rigidbody>();

        m_Player = FindObjectOfType<Movement_Player>();
        m_Player.AddEnemy(this);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
            m_IsKnockbacked = true;

        if (m_IsKnockbacked == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_SeekTarget.transform.position, m_Speed);
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);

            transform.LookAt(m_SeekTarget.transform);


            Debug.DrawRay(transform.position, transform.forward * 2f, Color.red);
            if (Physics.Raycast(transform.position, transform.forward, out m_Hit, 2f))
            {
                if (m_Hit.collider != null && m_Hit.collider.tag == "Player")
                {
                    SceneManager.LoadScene("DeathScene");
                    PlayerPrefs.SetInt("Score", m_Scores.m_Score);
                }
            }
        }
        else
        {
            StartCoroutine(KnockBack());
        }
    }

    private IEnumerator KnockBack()
    {
        m_KnockbackDir = m_SeekTarget.transform.position - transform.position;
        m_RB.AddForce(m_KnockbackDir.normalized * -m_MultiplierKnockback);
        yield return new WaitForSeconds(0.2f);
        m_RB.AddForce(m_KnockbackDir.normalized * m_MultiplierKnockback);
        m_IsKnockbacked = false;
    }

}



