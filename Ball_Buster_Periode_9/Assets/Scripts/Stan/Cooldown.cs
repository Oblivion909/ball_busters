﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooldown : MonoBehaviour
{
    [SerializeField] private float m_CooldownTimer;

    public bool m_Missed;


    void Update()
    {
        if (m_Missed == true)
        {
            m_CooldownTimer -= Time.deltaTime;
        }
    }
}



