﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    private float m_Width;
    private float m_Height;
    private float m_Depth;

    void Update()
    {
        m_Width = Screen.width;
        m_Height = Screen.height;
        


        transform.localScale = new Vector3(m_Width, m_Height, 1);
    }
}
