﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalingToScreen : MonoBehaviour
{
    public GameObject m_ObjectToLocateBottomRight;
    public GameObject m_ObjectToLocateTopLeft;
    public GameObject m_ObjectToLocateTopRight;
    public GameObject m_ObjectToLocateBottomLeft;
    [HideInInspector] public float m_Width;
    [HideInInspector] public float m_Depth;

    void Start()
    {
        m_Width = Screen.width;
        m_Depth = Screen.height;
    }

    void Update()
    {
        m_Width = Screen.width;
        m_Depth = Screen.height;

        m_ObjectToLocateTopLeft.transform.position = new Vector3(-m_Width / 100, transform.position.y, m_Depth / 100);
        m_ObjectToLocateTopRight.transform.position = new Vector3(m_Width / 100, transform.position.y, m_Depth / 100);
        m_ObjectToLocateBottomLeft.transform.position = new Vector3(-m_Width / 100, transform.position.y, -m_Depth / 100);
        m_ObjectToLocateBottomRight.transform.position = new Vector3(m_Width / 100, transform.position.y, -m_Depth / 100);
    }
}
