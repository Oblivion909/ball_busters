﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Movement
{
    public class GetScore : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_ScoreText;
        [SerializeField] private TextMeshProUGUI m_HighScoreText;

        #region Integers
        private int m_Score;
        private int m_CurrentHighscore;
        private int m_NewHighscore;
        #endregion

        #region Booleans
        private bool m_SetNewHighScore;
        private bool m_GetNewHighScore;
        private bool m_NewHighScoreText;
        #endregion

        void Start()
        {
            m_Score = PlayerPrefs.GetInt("Score");
            m_CurrentHighscore = PlayerPrefs.GetInt("Highscore");

            //Resets highscore, Run scene once\\
            //m_CurrentHighscore = 0;
        }

        void Update()
        {
            m_ScoreText.SetText(m_Score.ToString());

            #region PlayerPrefsForHighscore
            if (m_Score > m_CurrentHighscore)
            {
                // Set the current highscore to new highscore
                m_NewHighscore = m_Score;
                m_CurrentHighscore = m_NewHighscore;
                m_SetNewHighScore = true;
            }

            else if (m_Score <= m_CurrentHighscore)
            {
                // Keep highscore the same
                PlayerPrefs.SetInt("Highscore", m_CurrentHighscore);
            }
            
            if (m_SetNewHighScore)
            {
                PlayerPrefs.SetInt("Highscore", m_CurrentHighscore);
                m_SetNewHighScore = false;
                m_GetNewHighScore = true;
            }

            if (m_GetNewHighScore)
            {
                PlayerPrefs.GetInt("Highscore");
                m_GetNewHighScore = false;
                m_NewHighScoreText = true;
            }

            if(m_NewHighScoreText)
            {
                m_HighScoreText.SetText("New Highscore!!: " + m_CurrentHighscore.ToString());
            }
            else if(!m_NewHighScoreText)
            {
                m_HighScoreText.SetText("Highscore: " + m_CurrentHighscore.ToString());
            }
            #endregion
        }

    }
}
