﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    private Enemy_Health _HP;

    [HideInInspector] public int m_Score;
    public int m_ScoreAdded;

    [SerializeField] private TextMeshProUGUI m_ScoreText;

    void Start()
    {
        m_Score = 0;
    }

    void Update()
    {
        m_ScoreText.SetText(m_Score.ToString());
    }
}


