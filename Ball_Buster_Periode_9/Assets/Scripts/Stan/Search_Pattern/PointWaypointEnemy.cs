﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StateEnemy
{
    down = 0,
    middle,
    up
};
public class PointWaypointEnemy : MonoBehaviour
{
    [SerializeField]
    private StateEnemy m_State;

    [SerializeField]
    private GameObject[] m_Waypoints;

    public GameObject m_TargetWaypoint;

    private int m_NumState = 0;

    public bool m_AmIPaused = false;

    private void Start()
    {
        m_TargetWaypoint = m_Waypoints[0];
    }

    private void Update()
    {
        if (m_AmIPaused)
            return;

        if (m_NumState > 2)
        {
            m_State = 0;
            m_NumState = 0;
        }

        switch (m_State)
        {
            case StateEnemy.down:
                m_TargetWaypoint = m_Waypoints[0];
                RotateFinger(-35f);
                break;
            case StateEnemy.middle:
                RotateFinger(0f);
                m_TargetWaypoint = m_Waypoints[1];
                break;
            case StateEnemy.up:
                RotateFinger(35f);
                m_TargetWaypoint = m_Waypoints[2];
                break;
            default:
                m_State = StateEnemy.middle;
                break;
        }
    }

    private void RotateFinger(float degree)
    {
        transform.rotation = Quaternion.Euler(0, 180, degree);
    }

    private void OnMouseDown()
    {
        m_State += 1;
        m_NumState += 1;
    }
}
