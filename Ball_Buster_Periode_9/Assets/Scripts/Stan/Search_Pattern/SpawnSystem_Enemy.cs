﻿using UnityEngine;

public class SpawnSystem_Enemy : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Unit;
    [SerializeField] private Transform[] m_SpawnPos;

    [SerializeField] private int m_MaxAmountUnits;

    [SerializeField] private int m_TimeToSpawnMax;
    [SerializeField] private int m_TimeToSpawnMin;
    private float m_TargetTime;
    private float m_Timer;
    private int amountSpawns = 0;

    public bool m_AmIPaused = false;

    void Start()
    {
        m_TargetTime = Random.Range(m_TimeToSpawnMin, m_TimeToSpawnMax);
    }

    void Update()
    {
        if (m_AmIPaused)
            return;

        m_Timer += Time.deltaTime;

        if (m_Timer >= m_TargetTime && amountSpawns < m_MaxAmountUnits)
            ReachedTime();
    }

    /// <summary>
    /// Spawns at parent position a random unit at a random time
    /// </summary>
    private void ReachedTime()
    {
        amountSpawns += 1;
        Instantiate(m_Unit[Random.Range(0, m_Unit.Length)], m_SpawnPos[Random.Range(0, m_SpawnPos.Length)].position, m_SpawnPos[Random.Range(0, m_SpawnPos.Length)].rotation);
        m_TargetTime = Random.Range(m_TimeToSpawnMin, m_TimeToSpawnMax);
        m_Timer = 0;
    }

    /// <summary>
    /// Gets a new spawn pos when died
    /// </summary>
    /// <param name="enemy">The enemy that died</param>
    public void Respawn(Transform enemy)
    {
        Vector3 respawnPos;
        respawnPos = m_SpawnPos[Random.Range(0, m_SpawnPos.Length)].position;
        enemy.position = new Vector3(respawnPos.x, respawnPos.y, respawnPos.z);
    }
}
