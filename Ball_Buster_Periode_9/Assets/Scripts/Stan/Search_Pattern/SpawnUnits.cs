﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnUnits : MonoBehaviour
{
    [SerializeField] private GameObject m_Miner;
    [SerializeField] private GameObject[] m_Knights = new GameObject[3];

    // Start is called before the first frame update
    void Start()
    {
        if (m_Knights[0] == null || m_Miner == null)
            Debug.LogError("Prefab missing in " + gameObject.name);
    }

    public void SpawnMiner()
    {
        Instantiate(m_Miner, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
    }

    public void SpawnKnight1()
    {
        Instantiate(m_Knights[0], new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
    }

    public void SpawnKnight2()
    {
        Instantiate(m_Knights[1], new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
    }

    public void SpawnKnight3()
    {
        Instantiate(m_Knights[2], new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
    }
}
